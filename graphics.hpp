#pragma once
#include <gtkmm.h>
#include "lgs.hpp"

namespace Graphics {
	constexpr double PI = 3.1415926536;

	void draw_connection(
		const Cairo::RefPtr<Cairo::Context>& cr,
		int x1, int y1,
		int x2, int y2,
		bool active = false
	);

	void draw_connection(
		const Cairo::RefPtr<Cairo::Context>& cr,
		int x1, int y1,
		int x2, int y2,
		bool active,
		double size
	);

	void draw_connection_curved(
		const Cairo::RefPtr<Cairo::Context>& cr,
		int x1, int y1,
		int x2, int y2,
		bool active,
		double size
	);

	void draw_cell(
		const Cairo::RefPtr<Cairo::Context>& cr,
		Lgs::Type type,
		int x = 20, int y = 20,
		bool active = false
	);

	void draw_cell(
		const Cairo::RefPtr<Cairo::Context>& cr,
		Lgs::Type type,
		int x, int y,
		bool active,
		double size
	);

	bool draw_cell_input (const Cairo::RefPtr<Cairo::Context>& cr);
	bool draw_cell_output(const Cairo::RefPtr<Cairo::Context>& cr);
	bool draw_cell_and   (const Cairo::RefPtr<Cairo::Context>& cr);
	bool draw_cell_nand  (const Cairo::RefPtr<Cairo::Context>& cr);
	bool draw_cell_or    (const Cairo::RefPtr<Cairo::Context>& cr);
	bool draw_cell_nor   (const Cairo::RefPtr<Cairo::Context>& cr);
	bool draw_cell_xor   (const Cairo::RefPtr<Cairo::Context>& cr);
	bool draw_cell_xnor  (const Cairo::RefPtr<Cairo::Context>& cr);
	bool draw_cell_not   (const Cairo::RefPtr<Cairo::Context>& cr);
}
