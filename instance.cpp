#include <fstream>
#include "instance.hpp"
#include "graphics.hpp"

Instance::Data::Data(Root& root): Data(root, "") {}

Instance::Data::Data(Root& root, const std::string& filePath):
	root(root),
	signalChanged(),
	network(),
	selection(),
	saveFilePath(filePath),
	zoom(1.0),
	stickToGrid(true),
	curvedConnections(false),
	pointerOnGrab(false),
	pointerGrabPos{0, 0},
	pointerGrabOrigin{0, 0},
	view{0, 0},
	grabSelectionVector{0, 0},
	autoTickActive(false),
	autoTickDelay(400),
	isTickStable(false)
{
	if (saveFilePath.size() != 0) {
		std::ifstream file(saveFilePath);
		if (file.is_open()) {
			try {
				Json json(file);
				if (not network.load_from_json(json["cells"])) {
					throw Json::bad_syntax();
				}
				view.x = json["view x"].number();
				view.y = json["view y"].number();
			}
			catch (Json::bad_syntax&) {
				network.clear();
				view = {0, 0};
			}
			catch (Json::bad_type&) {
				network.clear();
				view = {0, 0};
			}
		}
	}

	signalChanged.connect(
		sigc::mem_fun(*this, &Instance::Data::update)
	);
}

Instance::Data::~Data() {
	autoTickConnection.disconnect();
}

void Instance::Data::update(Target target) {
	if (target & TARGET_NETWORK) {
		isTickStable = false;
	}
}

int Instance::Data::translate_pos_x_in(int x) {
	return x / zoom + view.x;
}
int Instance::Data::translate_pos_y_in(int y) {
	return y / zoom + view.y;
}
int Instance::Data::translate_pos_x_out(int x) {
	return (x - view.x) * zoom;
}
int Instance::Data::translate_pos_y_out(int y) {
	return (y - view.y) * zoom;
}

void Instance::Data::toggle_selected_cell_state() {
	if (selection.size() == 1) {
		network[*selection.begin()].set_state(
			not network[*selection.begin()].get_state()
		);
		signalChanged.emit(TARGET_NETWORK);
	}
}

void Instance::Data::set_auto_tick(bool active) {
	if (active) {
		if (not autoTickActive) {
			autoTickConnection = Glib::signal_timeout().connect(
				sigc::mem_fun(*this, &Instance::Data::auto_tick_routine),
				autoTickDelay
			);
			autoTickActive = true;
		}
	}
	else {
		if (autoTickActive) {
			autoTickConnection.disconnect();
			autoTickActive = false;
		}
	}
}

void Instance::Data::perform_tick() {
	if (not isTickStable) {
		if (network.perform_tick() > 0) {
			signalChanged.emit(TARGET_NETWORK);
		}
		else {
			isTickStable = true;
		}
	}
}

bool Instance::Data::auto_tick_routine() {
	if (autoTickActive) {
		perform_tick();
	}
	return autoTickActive;
}

void Instance::Data::clear() {
	saveFilePath.clear();
	selection.clear();
	network.clear();
	signalChanged.emit(TARGET_NETWORK | TARGET_SELECTION);
}

void Instance::Data::save() {
	if (saveFilePath.size() != 0) {
		std::ofstream file(saveFilePath);
		Json json = Json::Object({
			{"cells", Json::Null()},
			{"view x", Json::Number(view.x)},
			{"view y", Json::Number(view.y)}
		});
		network.save_to_json(json["cells"]);
		json.print(file, true);
	}
}

Instance::Instance(Root& root): Instance(root, "") {}

Instance::Instance(Root& root, const std::string& filePath):
	data(root, filePath),
	toolbar(data),
	placingArea(data),
	propertiesPanel(data)
{
	if (data.saveFilePath.size() > 0) {
		std::string fileName = data.saveFilePath;
		size_t pos = fileName.rfind('/');
		if (pos != std::string::npos) {
			fileName = fileName.substr(pos + 1);
		}
		pos = fileName.find(".json", fileName.size() - 5);
		if (pos != std::string::npos) {
			fileName = fileName.substr(0, pos);
		}
		tabLabelTitle.set_use_markup(false);
		tabLabelTitle.set_text(fileName);
	}
	else {
		tabLabelTitle.set_use_markup(true);
		tabLabelTitle.set_label("<span style=\"italic\">untitled</span>");
	}
	tabLabelClose.set_image_from_icon_name("gtk-close");
	tabLabelClose.set_relief(Gtk::RELIEF_NONE);
	tabLabel.pack_start(tabLabelTitle);
	tabLabel.pack_start(tabLabelClose);
	tabLabel.show_all_children();
	tabLabelClose.signal_clicked().connect(
		sigc::mem_fun(*this, &Instance::action_close)
	);

	/* drag and drop */

	std::vector<Gtk::TargetEntry> listTargets;
	listTargets.push_back(Gtk::TargetEntry("Lgs::Type"));

	toolbar.cellInput .drag_source_set(listTargets);
	toolbar.cellOutput.drag_source_set(listTargets);
	toolbar.cellAnd   .drag_source_set(listTargets);
	toolbar.cellNand  .drag_source_set(listTargets);
	toolbar.cellOr    .drag_source_set(listTargets);
	toolbar.cellNor   .drag_source_set(listTargets);
	toolbar.cellXor   .drag_source_set(listTargets);
	toolbar.cellXnor  .drag_source_set(listTargets);
	toolbar.cellNot   .drag_source_set(listTargets);
	placingArea.drag_dest_set(listTargets);

	toolbar.cellInput.signal_drag_data_get().connect(
		sigc::ptr_fun(&Toolbar::handler_for_cell_input)
	);
	toolbar.cellOutput.signal_drag_data_get().connect(
		sigc::ptr_fun(&Toolbar::handler_for_cell_output)
	);
	toolbar.cellAnd.signal_drag_data_get().connect(
		sigc::ptr_fun(&Toolbar::handler_for_cell_and)
	);
	toolbar.cellNand.signal_drag_data_get().connect(
		sigc::ptr_fun(&Toolbar::handler_for_cell_nand)
	);
	toolbar.cellOr.signal_drag_data_get().connect(
		sigc::ptr_fun(&Toolbar::handler_for_cell_or)
	);
	toolbar.cellNor.signal_drag_data_get().connect(
		sigc::ptr_fun(&Toolbar::handler_for_cell_nor)
	);
	toolbar.cellXor.signal_drag_data_get().connect(
		sigc::ptr_fun(&Toolbar::handler_for_cell_xor)
	);
	toolbar.cellXnor.signal_drag_data_get().connect(
		sigc::ptr_fun(&Toolbar::handler_for_cell_xnor)
	);
	toolbar.cellNot.signal_drag_data_get().connect(
		sigc::ptr_fun(&Toolbar::handler_for_cell_not)
	);
	placingArea.signal_drag_data_received().connect(
		sigc::mem_fun(placingArea, &PlacingArea::gates_drag_drop)
	);

	pack_start(toolbar, false, false);
	frame.set_border_width(4);
	frame.add(placingArea);
	pack_start(frame);
	pack_end(propertiesPanel, false, false);
	show_all_children();
	show();
}

void Instance::change_file_path(const std::string& filePath) {
	data.saveFilePath = filePath;
	if (filePath.size() > 0) {
		std::string fileName = filePath;
		size_t pos = fileName.rfind('/');
		if (pos != std::string::npos) {
			fileName = fileName.substr(pos + 1);
		}
		pos = fileName.find(".json", fileName.size() - 5);
		if (pos != std::string::npos) {
			fileName = fileName.substr(0, pos);
		}
		tabLabelTitle.set_use_markup(false);
		tabLabelTitle.set_text(fileName);
	}
	else {
		tabLabelTitle.set_use_markup(true);
		tabLabelTitle.set_label("<span style=\"italic\">untitled</span>");
	}
}

void Instance::action_close() {
	signalClose.emit(this);
}

Instance::PlacingArea::PlacingArea(Data& data): data(data) {
	set_can_focus();
	add_events(
		Gdk::BUTTON_PRESS_MASK |
		Gdk::BUTTON_RELEASE_MASK |
		Gdk::KEY_PRESS_MASK |
		Gdk::BUTTON_MOTION_MASK |
		Gdk::SCROLL_MASK
	);
	data.signalChanged.connect(
		sigc::mem_fun(*this, &Instance::PlacingArea::update)
	);
	update(Data::TARGET_SELECTION | Data::TARGET_NETWORK);
}

void Instance::PlacingArea::update(Data::Target target) {
	if (target != Data::TARGET_NONE) queue_draw();
}

bool Instance::PlacingArea::on_motion_notify_event(GdkEventMotion* event) {
	Data::Target target = Data::TARGET_NONE;
	if (event->state & GDK_BUTTON1_MASK and event->state & GDK_SHIFT_MASK) {
		if (data.selection.size() > 0) {
			int x = data.translate_pos_x_in(
				event->x + data.grabSelectionVector.x
			);
			int y = data.translate_pos_y_in(
				event->y + data.grabSelectionVector.y
			);
			if (data.stickToGrid) {
				x = x & ~0b111;
				y = y & ~0b111;
			}
			data.network.set_average_position(data.selection, x, y);
			target |= Data::TARGET_NETWORK;
		}
	}

	if (event->state & GDK_BUTTON2_MASK) {
		data.view.x += 2 * ((double)event->x - data.pointerGrabPos.x) / data.zoom;
		data.view.y += 2 * ((double)event->y - data.pointerGrabPos.y) / data.zoom;
		target |= Data::TARGET_VIEW;
	}

	data.pointerGrabPos = {(int)event->x, (int)event->y};
	target |= Data::TARGET_GRAB;

	if (target != Data::TARGET_NONE) {
		data.signalChanged.emit(target);
	}
	return true;
}

bool Instance::PlacingArea::on_key_press_event(GdkEventKey* event) {
	if (event->type == GDK_KEY_PRESS and event->keyval == GDK_KEY_Delete) {
		if (data.selection.size() > 0) {
			data.network.remove(data.selection);
			data.signalChanged.emit(
				Data::TARGET_SELECTION | Data::TARGET_NETWORK
			);
		}
	}
	else if (event->type == GDK_KEY_PRESS and event->keyval == GDK_KEY_Escape) {
		if (data.selection.size() > 0) {
			data.selection.clear();
			data.signalChanged.emit(Data::TARGET_SELECTION);
		}
	}
	else if (
		event->type == GDK_KEY_PRESS and
		event->keyval == GDK_KEY_c and
		event->state & GDK_CONTROL_MASK
	) {
		data.network.copy(data.selection, data.root.clipboard);
	}
	else if (
		event->type == GDK_KEY_PRESS and
		event->keyval == GDK_KEY_v and
		event->state & GDK_CONTROL_MASK
	) {
		int x = data.translate_pos_x_in(get_allocated_width() / 2);
		int y = data.translate_pos_y_in(get_allocated_height() / 2);
		data.network.paste(data.root.clipboard, x, y, &data.selection);

		data.signalChanged.emit(
			Data::TARGET_NETWORK | Data::TARGET_SELECTION
		);
	}
	return true;
}

bool Instance::PlacingArea::on_button_press_event(GdkEventButton* event) {
	grab_focus();
	Data::Target target = Data::TARGET_NONE;

	data.pointerGrabPos = {(int)event->x, (int)event->y};
	data.pointerGrabOrigin = {(int)event->x, (int)event->y};
	target |= Data::TARGET_GRAB;
	if (event->button == 1 && not (event->state & GDK_SHIFT_MASK)) {
		data.pointerOnGrab = true;
	}

	if (event->button == 1) {
		int x, y;
		data.network.get_average_position(data.selection, x, y);
		data.grabSelectionVector.x = data.translate_pos_x_out(x) - event->x;
		data.grabSelectionVector.y = data.translate_pos_y_out(y) - event->y;
	}

	if (
		event->type == GDK_BUTTON_PRESS and
		not (event->state & GDK_SHIFT_MASK) and
		event->button == 1
	) {
		if (event->state & GDK_CONTROL_MASK) {
			unsigned int clicked = data.network.find_at_pos(
				data.translate_pos_x_in(event->x),
				data.translate_pos_y_in(event->y)
			);
			if (clicked != Lgs::NONE) {
				if (data.selection.find(clicked) != data.selection.end()) {
					data.selection.erase(clicked);
				}
				else {
					data.selection.insert(clicked);
				}
				target |= Data::TARGET_SELECTION;
			}
		}
		else {
			unsigned int clicked = data.network.find_at_pos(
				data.translate_pos_x_in(event->x),
				data.translate_pos_y_in(event->y)
			);
			if (clicked == Lgs::NONE) {
				if (data.selection.size() > 0) {
					data.selection.clear();
					target |= Data::TARGET_SELECTION;
				}
			}
			else {
				switch (data.selection.size()) {
					case 0: {
						data.selection.insert(clicked);
						target |= Data::TARGET_SELECTION;
					} break;

					case 1: {
						if (*data.selection.begin() != clicked) {
							data.selection.clear();
							data.selection.insert(clicked);
							target |= Data::TARGET_SELECTION;
						}
					} break;

					default: {
						data.selection.clear();
						data.selection.insert(clicked);
						target |= Data::TARGET_SELECTION;
					} break;
				}
			}
		}
	}
	else if (
		event->type == GDK_2BUTTON_PRESS and
		event->button == 1 and
		not (event->state & GDK_CONTROL_MASK)
	) {
		unsigned int clicked = data.network.find_at_pos(
			data.translate_pos_x_in(event->x),
			data.translate_pos_y_in(event->y)
		);
		if (clicked != Lgs::NONE) {
			data.network[clicked].set_state(
				not data.network[clicked].get_state()
			);
			target |= Data::TARGET_NETWORK;
		}
	}
	else if (event->type == GDK_BUTTON_PRESS and event->button == 3) {
		if (event->state & GDK_SHIFT_MASK) {
			unsigned int cellId = data.network.find_at_pos(
				data.translate_pos_x_in(event->x),
				data.translate_pos_y_in(event->y)
			);
			if (cellId != Lgs::NONE) {
				Lgs::Cell& cellRef = data.network[cellId];
				switch (cellRef.type) {
					case Lgs::TYPE_INPUT: {
						data.network.disconnect_src(cellId);
					} break;

					case Lgs::TYPE_OUTPUT: {
						data.network.disconnect_dst(cellId);
						target |= Data::TARGET_NETWORK;
					} break;

					case Lgs::TYPE_NOT: {
						if (data.translate_pos_x_in(event->x) < cellRef.x) {
							data.network.disconnect_dst(cellId);
						}
						else {
							data.network.disconnect_src(cellId);
						}
						target |= Data::TARGET_NETWORK;
					} break;

					case Lgs::TYPE_AND:
					case Lgs::TYPE_NAND:
					case Lgs::TYPE_OR:
					case Lgs::TYPE_NOR:
					case Lgs::TYPE_XOR:
					case Lgs::TYPE_XNOR: {
						if (data.translate_pos_x_in(event->x) < cellRef.x) {
							if (data.translate_pos_y_in(event->y) < cellRef.y) {
								data.network.disconnect_dst(cellId, 0);
							}
							else {
								data.network.disconnect_dst(cellId, 1);
							}
						}
						else {
							data.network.disconnect_src(cellId);
						}
						target |= Data::TARGET_NETWORK;
					} break;
				}
			}
		}
		else {
			if (data.selection.size() == 1) {
				unsigned int clicked = data.network.find_at_pos(
					data.translate_pos_x_in(event->x),
					data.translate_pos_y_in(event->y)
				);
				if (clicked != Lgs::NONE) {
					switch (data.network[clicked].type) {
						case Lgs::TYPE_INPUT: {
						} break;

						case Lgs::TYPE_OUTPUT:
						case Lgs::TYPE_NOT: {
							data.network.connect(
								*data.selection.begin(), clicked
							);
							target |= Data::TARGET_NETWORK;
						} break;

						case Lgs::TYPE_AND:
						case Lgs::TYPE_NAND:
						case Lgs::TYPE_OR:
						case Lgs::TYPE_NOR:
						case Lgs::TYPE_XOR:
						case Lgs::TYPE_XNOR: {
							if (
								data.translate_pos_y_in(event->y) <
								data.network[clicked].y
							) {
								data.network.connect(
									*data.selection.begin(), clicked, 0
								);
							}
							else {
								data.network.connect(
									*data.selection.begin(), clicked, 1
								);
							}
							target |= Data::TARGET_NETWORK;
						} break;
					}
				}
			}
		}
	}

	if (target != Data::TARGET_NONE) {
		data.signalChanged.emit(target);
	}

	return true;
}

bool Instance::PlacingArea::on_button_release_event(GdkEventButton* event) {
	Data::Target target = Data::TARGET_NONE;

	if (data.pointerOnGrab) {
		data.pointerOnGrab = false;
		target |= Data::TARGET_GRAB;

		if (
			std::abs(
				(int)data.pointerGrabPos.x - (int)data.pointerGrabOrigin.x
			) > 4 ||
			std::abs(
				(int)data.pointerGrabPos.y - (int)data.pointerGrabOrigin.y
			) > 4
		) {
			if (!(event->state & GDK_CONTROL_MASK)) {
				data.selection.clear();
			}
			for (auto& it : data.network) {
				if (it.second.is_in(
					data.translate_pos_x_in(data.pointerGrabPos.x),
					data.translate_pos_y_in(data.pointerGrabPos.y),
					data.translate_pos_x_in(data.pointerGrabOrigin.x),
					data.translate_pos_y_in(data.pointerGrabOrigin.y)
				)) {
					data.selection.insert(it.first);
				}
			}
			target |= Data::TARGET_SELECTION;
		}
	}

	if (target != Data::TARGET_NONE) {
		data.signalChanged.emit(target);
	}
	return true;
}

bool Instance::PlacingArea::on_scroll_event(GdkEventScroll* event) {
	if (event->state & GDK_CONTROL_MASK) {
		if (event->direction == GDK_SCROLL_UP) {
			if (data.zoom < 2) {
				data.view.x += event->x / (4 * data.zoom * (data.zoom + 0.25));
				data.view.y += event->y / (4 * data.zoom * (data.zoom + 0.25));
				data.zoom += 0.25;
			}
		}
		else if (event->direction == GDK_SCROLL_DOWN) {
			if (data.zoom >= 0.5) {
				data.view.x -= event->x / (4 * data.zoom * (data.zoom - 0.25));
				data.view.y -= event->y / (4 * data.zoom * (data.zoom - 0.25));
				data.zoom -= 0.25;
			}
		}
		data.signalChanged.emit(Data::TARGET_VIEW);
	}
	return true;
}

bool Instance::PlacingArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
	if (data.pointerOnGrab) {
		cr->move_to(data.pointerGrabOrigin.x, data.pointerGrabOrigin.y);
		cr->line_to(data.pointerGrabPos.x, data.pointerGrabOrigin.y);
		cr->line_to(data.pointerGrabPos.x, data.pointerGrabPos.y);
		cr->line_to(data.pointerGrabOrigin.x, data.pointerGrabPos.y);
		cr->close_path();
		cr->set_source_rgb(0.8, 0.8, 0.8);
		cr->fill_preserve();
		cr->set_source_rgb(0.4, 0.4, 0.4);
		cr->set_line_width(1);
		cr->stroke();
	}

	for (auto& it : data.selection) {
		cr->set_line_width(2);
		cr->move_to(
			data.translate_pos_x_out(data.network[it].x) - 13 * data.zoom,
			data.translate_pos_y_out(data.network[it].y) - 13 * data.zoom
		);
		cr->rel_line_to(26 * data.zoom, 0);
		cr->rel_line_to(0, 26 * data.zoom);
		cr->rel_line_to(-26 * data.zoom, 0);
		cr->close_path();
		cr->set_source_rgb(0.9, 0.9, 0.9);
		cr->fill_preserve();
		cr->set_source_rgb(0.7, 0.7, 0.7);
		cr->stroke();
	}

	cr->set_line_width(2);
	cr->set_source_rgb(0.5, 0.5, 0.5);

	for (auto& it : data.network) {
		Lgs::Cell& cell = it.second;
		switch (cell.type) {
			case Lgs::TYPE_INPUT: {
			} break;

			case Lgs::TYPE_OUTPUT:
			case Lgs::TYPE_NOT: {
				if (cell.inputA.ptr != nullptr) {
					if (data.curvedConnections) {
						Graphics::draw_connection_curved(
							cr,
							data.translate_pos_x_out(cell.x),
							data.translate_pos_y_out(cell.y),
							data.translate_pos_x_out(cell.inputA.ptr->x),
							data.translate_pos_y_out(cell.inputA.ptr->y),
							cell.inputA.ptr->get_state(),
							data.zoom
						);
					}
					else {
						Graphics::draw_connection(
							cr,
							data.translate_pos_x_out(cell.x),
							data.translate_pos_y_out(cell.y),
							data.translate_pos_x_out(cell.inputA.ptr->x),
							data.translate_pos_y_out(cell.inputA.ptr->y),
							cell.inputA.ptr->get_state(),
							data.zoom
						);
					}
				}
			} break;

			case Lgs::TYPE_AND:
			case Lgs::TYPE_NAND:
			case Lgs::TYPE_OR:
			case Lgs::TYPE_NOR:
			case Lgs::TYPE_XOR:
			case Lgs::TYPE_XNOR: {
				if (cell.inputA.ptr != nullptr) {
					if (data.curvedConnections) {
						Graphics::draw_connection_curved(
							cr,
							data.translate_pos_x_out(cell.x),
							data.translate_pos_y_out(cell.y - 5),
							data.translate_pos_x_out(cell.inputA.ptr->x),
							data.translate_pos_y_out(cell.inputA.ptr->y),
							cell.inputA.ptr->get_state(),
							data.zoom
						);
					}
					else {
						Graphics::draw_connection(
							cr,
							data.translate_pos_x_out(cell.x),
							data.translate_pos_y_out(cell.y - 5),
							data.translate_pos_x_out(cell.inputA.ptr->x),
							data.translate_pos_y_out(cell.inputA.ptr->y),
							cell.inputA.ptr->get_state(),
							data.zoom
						);
					}
				}
				if (cell.inputB.ptr != nullptr) {
					if (data.curvedConnections) {
						Graphics::draw_connection_curved(
							cr,
							data.translate_pos_x_out(cell.x),
							data.translate_pos_y_out(cell.y + 5),
							data.translate_pos_x_out(cell.inputB.ptr->x),
							data.translate_pos_y_out(cell.inputB.ptr->y),
							cell.inputB.ptr->get_state(),
							data.zoom
						);
					}
					else {
						Graphics::draw_connection(
							cr,
							data.translate_pos_x_out(cell.x),
							data.translate_pos_y_out(cell.y + 5),
							data.translate_pos_x_out(cell.inputB.ptr->x),
							data.translate_pos_y_out(cell.inputB.ptr->y),
							cell.inputB.ptr->get_state(),
							data.zoom
						);
					}
				}
			} break;
		}
	}

	for (auto& it : data.network) {
		Lgs::Cell& cell = it.second;
		Graphics::draw_cell(
			cr,
			cell.type,
			data.translate_pos_x_out(cell.x),
			data.translate_pos_y_out(cell.y),
			cell.get_state(),
			data.zoom
		);
	}
	return true;
}

Instance::PropertiesPanel::PropertiesPanel(Data& data): data(data) {
	set_sensitive(false);
	set_size_request(160);
	set_margin_left  (8);
	set_margin_right (8);
	set_margin_top   (8);
	set_margin_bottom(8);
	set_spacing(8);

	grid.set_label("stick to grid");
	grid.set_active(data.stickToGrid);
	curve.set_label("curved connections");
	curve.set_active(data.curvedConnections);

	tickPerform.set_image_from_icon_name("gtk-media-forward");
	tickPlay.set_image_from_icon_name("gtk-media-play");
	tickEntry.set_range(10, 1000);
	tickEntry.set_increments(10, 100);
	tickEntry.set_value(data.autoTickDelay);
	separator1.set_margin_top(10);
	separator1.set_margin_bottom(10);
	separator2.set_margin_top(10);
	separator2.set_margin_bottom(10);

	label.set_use_markup(true);
	label.set_label("<b>CELL</b> none");
	label.set_selectable(true);
	remove.set_label("remove");
	posX.set_range(-10000, 10000);
	posY.set_range(-10000, 10000);
	if (data.stickToGrid) {
		posX.set_increments(8, 32);
		posY.set_increments(8, 32);
	}
	else {
		posX.set_increments(1, 10);
		posY.set_increments(1, 10);
	}

	tickBox.pack_start(tickPerform, false, false);
	tickBox.pack_start(tickPlay, false, false);
	tickBox.pack_start(tickEntry, false, false);
	pack_start(tickBox      , false, false);
	pack_start(separator2   , false, false);
	pack_start(label        , false, false);
	pack_start(posX         , false, false);
	pack_start(posY         , false, false);
	pack_start(stateSwitcher, false, false);
	pack_start(inputA       , false, false);
	pack_start(inputB       , false, false);
	pack_start(remove       , false, false);
	pack_start(separator1   , false, false);
	pack_start(grid         , false, false);
	pack_start(curve        , false, false);

	posX.signal_value_changed().connect(
		sigc::mem_fun(*this, &Instance::PropertiesPanel::change_pos_x)
	);
	posY.signal_value_changed().connect(
		sigc::mem_fun(*this, &Instance::PropertiesPanel::change_pos_y)
	);
	stateSwitcher.signal_button_release_event().connect(
		sigc::mem_fun(*this, &Instance::PropertiesPanel::change_state)
	);
	inputA.signal_activate().connect(
		sigc::mem_fun(*this, &Instance::PropertiesPanel::change_input_a)
	);
	inputB.signal_activate().connect(
		sigc::mem_fun(*this, &Instance::PropertiesPanel::change_input_b)
	);
	remove.signal_clicked().connect(
		sigc::mem_fun(*this, &Instance::PropertiesPanel::change_remove)
	);
	tickPerform.signal_clicked().connect(
		sigc::mem_fun(this->data, &Instance::Data::perform_tick)
	);
	tickPlay.signal_clicked().connect(
		sigc::mem_fun(*this, &Instance::PropertiesPanel::change_tick_toggle)
	);
	tickEntry.signal_value_changed().connect(
		sigc::mem_fun(*this, &Instance::PropertiesPanel::change_tick_delay)
	);
	grid.signal_toggled().connect(
		sigc::mem_fun(*this, &Instance::PropertiesPanel::change_stick_grid)
	);
	curve.signal_toggled().connect(
		sigc::mem_fun(*this, &Instance::PropertiesPanel::change_curve)
	);

	data.signalChanged.connect(
		sigc::mem_fun(*this, &Instance::PropertiesPanel::update)
	);

	set_sensitive(true);
	update(Data::TARGET_SELECTION | Data::TARGET_NETWORK);
}

void Instance::PropertiesPanel::change_stick_grid() {
	data.stickToGrid = grid.get_active();
	if (data.stickToGrid) {
		posX.set_increments(8, 32);
		posY.set_increments(8, 32);
	}
	else {
		posX.set_increments(1, 10);
		posY.set_increments(1, 10);
	}
}

void Instance::PropertiesPanel::change_curve() {
	data.curvedConnections = curve.get_active();
	data.signalChanged.emit(Data::TARGET_VIEW);
}

void Instance::PropertiesPanel::change_tick_toggle() {
	if (data.autoTickActive) {
		data.set_auto_tick(false);
		tickPlay.set_image_from_icon_name("gtk-media-play");
	}
	else {
		data.set_auto_tick(true);
		tickPlay.set_image_from_icon_name("gtk-media-pause");
	}
}

void Instance::PropertiesPanel::change_tick_delay() {
	data.autoTickDelay = tickEntry.get_value();
	if (data.autoTickActive) {
		data.set_auto_tick(false);
		data.set_auto_tick(true);
	}
}

void Instance::PropertiesPanel::change_pos_x() {
	if (data.selection.size() == 1) {
		data.network[*data.selection.begin()].x = posX.get_value();
		data.signalChanged.emit(Data::TARGET_NETWORK);
	}
	else if (data.selection.size() > 1) {
		data.network.set_average_position(
			data.selection,
			posX.get_value(),
			posY.get_value()
		);
		data.signalChanged.emit(Data::TARGET_NETWORK);
	}
}

void Instance::PropertiesPanel::change_pos_y() {
	if (data.selection.size() == 1) {
		data.network[*data.selection.begin()].y = posY.get_value();
		data.signalChanged.emit(Data::TARGET_NETWORK);
	}
	else if (data.selection.size() > 1) {
		data.network.set_average_position(
			data.selection,
			posX.get_value(),
			posY.get_value()
		);
		data.signalChanged.emit(Data::TARGET_NETWORK);
	}
}

bool Instance::PropertiesPanel::change_state(const GdkEventButton*) {
	if (data.selection.size() == 1) {
		data.network[*data.selection.begin()].set_state(not stateSwitcher.get_state());
		data.signalChanged.emit(Data::TARGET_NETWORK);
	}
	return false;
}

void Instance::PropertiesPanel::change_input_a() {
	if (data.selection.size() == 1) {
		char *ptr = nullptr;
		const char *str = inputA.get_text().data();
		unsigned long id = std::strtoul(str, &ptr, 10);
		if (
			ptr != nullptr and
			ptr != str and
			*ptr == '\0'
		) {
			data.network.connect(id, *data.selection.begin(), 0);
		}
		else {
			data.network.disconnect_dst(*data.selection.begin(), 0);
		}
		data.signalChanged.emit(Data::TARGET_NETWORK);
	}
}

void Instance::PropertiesPanel::change_input_b() {
	if (data.selection.size() == 1) {
		char* ptr = nullptr;
		const char* str = inputB.get_text().data();
		unsigned long id = std::strtoul(str, &ptr, 10);
		if (
			ptr != nullptr and
			ptr != str and
			*ptr == '\0'
		) {
			data.network.connect(id, *data.selection.begin(), 1);
		}
		else {
			data.network.disconnect_dst(*data.selection.begin(), 1);
		}
		data.signalChanged.emit(Data::TARGET_NETWORK);
	}
}

void Instance::PropertiesPanel::change_remove() {
	if (data.selection.size() > 0) {
		for (auto& it : data.selection) {
			data.network.remove(it);
		}
		data.selection.clear();
		data.signalChanged.emit(Data::TARGET_NETWORK | Data::TARGET_SELECTION);
	}
}

void Instance::PropertiesPanel::update(Data::Target target) {
	if ((target & Data::TARGET_NETWORK) or (target & Data::TARGET_SELECTION)) {
		if (data.selection.size() == 0) {
			label.set_label("<b>CELL</b> none");
			posX.set_value(0);
			posY.set_value(0);
			inputA.set_text("");
			inputB.set_text("");
			posX.set_sensitive(false);
			posY.set_sensitive(false);
			stateSwitcher.set_sensitive(false);
			inputA.set_sensitive(false);
			inputB.set_sensitive(false);
			remove.set_sensitive(false);
		}
		else if (data.selection.size() == 1) {
			Lgs::Cell& cell = data.network[*data.selection.begin()];
			label.set_label(
				"<b>CELL</b> " + std::to_string(*data.selection.begin())
			);
			posX.set_value(cell.x);
			posY.set_value(cell.y);
			stateSwitcher.set_state(cell.get_state());
			switch (cell.type) {
				case Lgs::TYPE_INPUT: {
					inputA.set_text("");
					inputA.set_sensitive(false);
					inputB.set_text("");
					inputB.set_sensitive(false);
				} break;

				case Lgs::TYPE_OUTPUT:
				case Lgs::TYPE_NOT: {
					if (cell.inputA.id != Lgs::NONE) {
						inputA.set_text(std::to_string(cell.inputA.id));
					}
					else {
						inputA.set_text("");
					}
					inputA.set_sensitive(true);
					inputB.set_text("");
					inputB.set_sensitive(false);
				} break;

				case Lgs::TYPE_AND:
				case Lgs::TYPE_NAND:
				case Lgs::TYPE_OR:
				case Lgs::TYPE_NOR:
				case Lgs::TYPE_XOR:
				case Lgs::TYPE_XNOR: {
					if (cell.inputA.id != Lgs::NONE) {
						inputA.set_text(std::to_string(cell.inputA.id));
					}
					else {
						inputA.set_text("");
					}
					inputA.set_sensitive(true);

					if (cell.inputB.id != Lgs::NONE) {
						inputB.set_text(std::to_string(cell.inputB.id));
					}
					else {
						inputB.set_text("");
					}
					inputB.set_sensitive(true);
				} break;
			}
			posX.set_sensitive(true);
			posY.set_sensitive(true);
			stateSwitcher.set_sensitive(true);
			remove.set_sensitive(true);
		}
		else {
			label.set_label("<b>CELL</b> multiple");
			int x, y;
			data.network.get_average_position(data.selection, x, y);
			posX.set_value(x);
			posY.set_value(y);
			inputA.set_text("");
			inputB.set_text("");
			posX.set_sensitive(true);
			posY.set_sensitive(true);
			stateSwitcher.set_sensitive(false);
			inputA.set_sensitive(false);
			inputB.set_sensitive(false);
			remove.set_sensitive(true);
		}
	}
}

void Instance::PlacingArea::gates_drag_drop(
	const Glib::RefPtr<Gdk::DragContext>& context,
	int x, int y,
	const Gtk::SelectionData& received,
	guint, guint time
) {
	grab_focus();
	if (
		received.get_target() == "Lgs::Type" &&
		received.get_format() == 8 * sizeof(Lgs::Type) &&
		received.get_length() == sizeof(Lgs::Type)
	) {
		x = data.translate_pos_x_in(x);
		y = data.translate_pos_y_in(y);
		if (data.stickToGrid) {
			x = x & ~0b111;
			y = y & ~0b111;
		}
		data.selection.clear();
		data.selection.insert(data.network.add(
			*((Lgs::Type*)received.get_data()),
			x, y
		));
		data.signalChanged.emit(
			Data::TARGET_NETWORK | Data::TARGET_SELECTION
		);
	}
	else {
		std::cerr << "drag action failed due to invalid format" << std::endl;
	}
	context->drag_finish(false, false, time);
}

Instance::Toolbar::Toolbar(Data& data): data(data) {
	cellInput.signal_draw().connect(
		sigc::ptr_fun(Graphics::draw_cell_input)
	);
	cellOutput.signal_draw().connect(
		sigc::ptr_fun(Graphics::draw_cell_output)
	);
	cellAnd.signal_draw().connect(
		sigc::ptr_fun(Graphics::draw_cell_and)
	);
	cellNand.signal_draw().connect(
		sigc::ptr_fun(Graphics::draw_cell_nand)
	);
	cellOr.signal_draw().connect(
		sigc::ptr_fun(Graphics::draw_cell_or)
	);
	cellNor.signal_draw().connect(
		sigc::ptr_fun(Graphics::draw_cell_nor)
	);
	cellXor.signal_draw().connect(
		sigc::ptr_fun(Graphics::draw_cell_xor)
	);
	cellXnor.signal_draw().connect(
		sigc::ptr_fun(Graphics::draw_cell_xnor)
	);
	cellNot.signal_draw().connect(
		sigc::ptr_fun(Graphics::draw_cell_not)
	);
	cellInput .set_size_request(40, 40);
	cellOutput.set_size_request(40, 40);
	cellAnd   .set_size_request(40, 40);
	cellNand  .set_size_request(40, 40);
	cellOr    .set_size_request(40, 40);
	cellNor   .set_size_request(40, 40);
	cellXor   .set_size_request(40, 40);
	cellXnor  .set_size_request(40, 40);
	cellNot   .set_size_request(40, 40);

	pack_start(cellInput , false, false);
	pack_start(cellOutput, false, false);
	pack_start(cellAnd   , false, false);
	pack_start(cellNand  , false, false);
	pack_start(cellOr    , false, false);
	pack_start(cellNor   , false, false);
	pack_start(cellXor   , false, false);
	pack_start(cellXnor  , false, false);
	pack_start(cellNot   , false, false);
	set_margin_left  (2);
	set_margin_right (2);
	set_margin_top   (2);
	set_margin_bottom(2);
	set_spacing(2);
}


Instance::Data::Target operator|(
	Instance::Data::Target a, Instance::Data::Target b
) {
	return (Instance::Data::Target)((unsigned long)a | (unsigned long)b);
}

Instance::Data::Target& operator|=(
	Instance::Data::Target& a, Instance::Data::Target b
) {
	a = (Instance::Data::Target)((unsigned long)a | (unsigned long)b);
	return a;
}
