#include <gtkmm.h>
#include <fstream>
#include <memory>
#include "root.hpp"
#include "instance.hpp"
#include "graphics.hpp"


class MainWindow: public Gtk::Window {
public:
	Root root;
	std::vector<std::unique_ptr<Instance>> instances;

	Gtk::VBox vbox;
	Gtk::Notebook tabs;

	Gtk::MenuBar menuBar;

	/* menu bar */
	Gtk::MenuItem menuBarFile;
	Gtk::MenuItem menuBarEdit;
	Gtk::MenuItem menuBarView;

	/* menu bar File */
	Gtk::Menu menuBarFileSubMenu;
	Gtk::ImageMenuItem menuBarFileNew;
	Gtk::ImageMenuItem menuBarFileOpen;
	Gtk::ImageMenuItem menuBarFileSave;
	Gtk::ImageMenuItem menuBarFileClose;

	Instance* get_active_instance() {
		return (Instance*)tabs.get_nth_page(tabs.get_current_page());
	}

	void action_open() {
		Gtk::FileChooserDialog dialog(
			"open lgs network file",
			Gtk::FILE_CHOOSER_ACTION_OPEN
		);
		dialog.set_transient_for(*this);
		dialog.set_icon_name("gtk-open");
		dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
		dialog.add_button("_Open", Gtk::RESPONSE_OK);
		dialog.set_filename("networks/*");
		switch (dialog.run()) {
			case Gtk::RESPONSE_OK: {
				Instance* instance = new Instance(root, dialog.get_filename());
				instances.push_back(std::unique_ptr<Instance>(instance));
				instance->signalClose.connect(
					sigc::mem_fun(*this, &MainWindow::action_close_specified)
				);
				tabs.set_current_page(
					tabs.append_page(*instance, instance->tabLabel)
				);
			} break;

			default: {
			} break;
		}
	}

	void action_save() {
		Instance* instance = get_active_instance();
		if (instance == nullptr) return;
		if (instance->data.saveFilePath.size() == 0) {
			Gtk::FileChooserDialog dialog(
				"save lgs network to file",
				Gtk::FILE_CHOOSER_ACTION_SAVE
			);
			dialog.set_transient_for(*this);
			dialog.set_icon_name("gtk-save");
			dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
			dialog.add_button("_Save", Gtk::RESPONSE_OK);
			dialog.set_filename("networks/*");
			switch (dialog.run()) {
				case Gtk::RESPONSE_OK: {
					instance->change_file_path(dialog.get_filename());
				} break;

				default: {
				} break;
			}
		}
		instance->data.save();
	}

	void action_new() {
		Instance* instance = new Instance(root);
		instances.push_back(std::unique_ptr<Instance>(instance));
		instance->signalClose.connect(
			sigc::mem_fun(*this, &MainWindow::action_close_specified)
		);
		tabs.set_current_page(
			tabs.append_page(*instance, instance->tabLabel)
		);
	}

	void action_close() {
		action_close_specified(get_active_instance());
	}

	void action_close_specified(Instance* instance) {
		if (instance == nullptr) return;
		tabs.remove_page(*instance);
		for (auto it = instances.begin(); it != instances.end(); it++) {
			if (it->get() == instance) {
				instances.erase(it);
				return;
			}
		}
		throw std::exception();
	}

	MainWindow() {
		set_title("bluestone");
		set_default_size(1280, 720);
		set_icon(Gdk::Pixbuf::create_from_file("bluestone.svg"));

		add(vbox);
		vbox.pack_start(menuBar, false, false);
		vbox.pack_start(tabs);

		menuBarFile.set_label("File");
		menuBarEdit.set_label("Edit");
		menuBarView.set_label("View");
		menuBar.append(menuBarFile);
		menuBar.append(menuBarEdit);
		menuBar.append(menuBarView);

		menuBarFileNew   = Gtk::ImageMenuItem(Gtk::Stock::NEW  );
		menuBarFileOpen  = Gtk::ImageMenuItem(Gtk::Stock::OPEN );
		menuBarFileSave  = Gtk::ImageMenuItem(Gtk::Stock::SAVE );
		menuBarFileClose = Gtk::ImageMenuItem(Gtk::Stock::CLOSE);
		menuBarFileSubMenu.append(menuBarFileNew  );
		menuBarFileSubMenu.append(menuBarFileOpen );
		menuBarFileSubMenu.append(menuBarFileSave );
		menuBarFileSubMenu.append(menuBarFileClose);
		menuBarFile.set_submenu(menuBarFileSubMenu);

		menuBarFileOpen.signal_activate().connect(
			sigc::mem_fun(*this, &MainWindow::action_open)
		);
		menuBarFileSave.signal_activate().connect(
			sigc::mem_fun(*this, &MainWindow::action_save)
		);
		menuBarFileNew.signal_activate().connect(
			sigc::mem_fun(*this, &MainWindow::action_new)
		);
		menuBarFileClose.signal_activate().connect(
			sigc::mem_fun(*this, &MainWindow::action_close)
		);

		show_all_children();
	}
};

int main(int argc, char* argv[]) {
	Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(
		argc, argv, "gtkmm.bluestone"
	);
	MainWindow window;
	return app->run(window);
}
