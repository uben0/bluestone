#include "graphics.hpp"

namespace Graphics {
	void draw_connection(const Cairo::RefPtr<Cairo::Context>& cr, int x1, int y1, int x2, int y2, bool active) {
		if (x2 + 30 > x1) {
			int yMin = (y1<y2 ? y1:y2) + 20;
			cr->move_to(x1, y1);
			cr->line_to(x1 - 15, y1);
			cr->line_to(x1 - 15, yMin);
			cr->line_to(x2 + 15, yMin);
			cr->line_to(x2 + 15, y2);
			cr->line_to(x2, y2);
		}
		else {
			cr->move_to(x1, y1);
			cr->line_to((x1 + x2) / 2, y1);
			cr->line_to((x1 + x2) / 2, y2);
			cr->line_to(x2, y2);
		}
		if (active) {
			cr->set_source_rgb(0.4, 0.7, 0.9);
		}
		else {
			cr->set_source_rgb(0.5, 0.5, 0.5);
		}
		cr->stroke();
	}

	void draw_connection(const Cairo::RefPtr<Cairo::Context>& cr, int x1, int y1, int x2, int y2, bool active, double size) {
		if (x2 + 30 * size > x1) {
			int yMin = (y1<y2 ? y1:y2) + 20 * size;
			cr->move_to(x1, y1);
			cr->line_to(x1 - 15 * size, y1);
			cr->line_to(x1 - 15 * size, yMin);
			cr->line_to(x2 + 15 * size, yMin);
			cr->line_to(x2 + 15 * size, y2);
			cr->line_to(x2, y2);
		}
		else {
			cr->move_to(x1, y1);
			cr->line_to((x1 + x2) / 2, y1);
			cr->line_to((x1 + x2) / 2, y2);
			cr->line_to(x2, y2);
		}
		if (active) {
			cr->set_source_rgb(0.4, 0.7, 0.9);
		}
		else {
			cr->set_source_rgb(0.5, 0.5, 0.5);
		}
		cr->stroke();
	}

	void draw_connection_curved(const Cairo::RefPtr<Cairo::Context>& cr, int x1, int y1, int x2, int y2, bool active, double size) {
		if (active) {
			cr->set_source_rgb(0.4, 0.7, 0.9);
		}
		else {
			cr->set_source_rgb(0.5, 0.5, 0.5);
		}
		if (x2 + 30 * size > x1) {
			if (y1 < y2) {
				cr->arc(x1, y1 + 18 * size, 18 * size, PI, 1.5 * PI);
				cr->stroke();
				cr->arc(x2, y2 - 18 * size, 18 * size, 0, 0.5 * PI);
				cr->stroke();

				x1 -= 18 * size;
				y1 += 18 * size;
				x2 += 18 * size;
				y2 -= 18 * size;
				int w = std::abs(x1 - x2);
				int h = std::abs(y1 - y2);
				int s = h + w;
				cr->move_to(x1, y1);
				cr->curve_to(
					x1, y1 + (h + 30 * size) * ((double)w / s),
					x2, y2 - (h + 30 * size) * ((double)w / s),
					x2, y2
				);
			}
			else {
				cr->arc(x1, y1 - 18 * size, 18 * size, 0.5 * PI, PI);
				cr->stroke();
				cr->arc(x2, y2 + 18 * size, 18 * size, 1.5 * PI, 2 * PI);
				cr->stroke();

				x1 -= 18 * size;
				y1 -= 18 * size;
				x2 += 18 * size;
				y2 += 18 * size;
				int w = std::abs(x1 - x2);
				int h = std::abs(y1 - y2);
				int s = h + w;
				cr->move_to(x1, y1);
				cr->curve_to(
					x1, y1 - (h + 30 * size) * ((double)w / s),
					x2, y2 + (h + 30 * size) * ((double)w / s),
					x2, y2
				);
			}
		}
		else {
			int w = std::abs(x1 - x2);
			int h = std::abs(y1 - y2);
			int s = h + w;
			cr->move_to(x1, y1);
			cr->curve_to(
				x1 - (w + 30 * size) * ((double)h / s), y1,
				x2 + (w + 30 * size) * ((double)h / s), y2,
				x2, y2
			);
		}
		cr->stroke();
	}

	void draw_cell(
		const Cairo::RefPtr<Cairo::Context>& cr,
		Lgs::Type type,
		int x, int y,
		bool active
	) {
		switch (type) {
			case Lgs::TYPE_INPUT: {
				cr->move_to(x - 10, y - 10);
				cr->rel_line_to(0, 20);
				cr->rel_line_to(20, 0);
				cr->rel_line_to(0, -20);
				cr->close_path();
			} break;

			case Lgs::TYPE_OUTPUT: {
				cr->arc(x, y, 10, 0, 2 * PI);
			} break;

			case Lgs::TYPE_AND: {
				cr->move_to(x, y - 10);
				cr->rel_line_to(-10, 0);
				cr->rel_line_to(0, 20);
				cr->arc_negative(x, y, 10, 0.5 * PI, 1.5 * PI);
			} break;

			case Lgs::TYPE_NAND: {
				cr->move_to(x, y - 10);
				cr->rel_line_to(-10, 0);
				cr->rel_line_to(0, 20);
				cr->arc_negative(x, y, 10, 0.5 * PI, 1.5 * PI);
				if (active) {
					cr->set_source_rgb(0.6, 0.7, 1.0);
				}
				else {
					cr->set_source_rgb(0.7, 0.7, 0.7);
				}
				cr->fill_preserve();
				cr->set_source_rgb(0.4, 0.4, 0.4);
				cr->stroke();
				cr->arc(x + 14, y, 4, PI, 3 * PI);
			} break;

			case Lgs::TYPE_OR: {
				cr->move_to(x - 10, y - 10);
				cr->rel_curve_to(5, 5, 5, 15, 0, 20);
				cr->rel_curve_to(10, 0, 20, -5, 20, -10);
				cr->rel_curve_to(0, -5, -10, -10, -20, -10);
				cr->close_path();
			} break;

			case Lgs::TYPE_NOR: {
				cr->move_to(x - 10, y - 10);
				cr->rel_curve_to(5, 5, 5, 15, 0, 20);
				cr->rel_curve_to(10, 0, 20, -5, 20, -10);
				cr->rel_curve_to(0, -5, -10, -10, -20, -10);
				cr->close_path();
				if (active) {
					cr->set_source_rgb(0.6, 0.7, 1.0);
				}
				else {
					cr->set_source_rgb(0.7, 0.7, 0.7);
				}
				cr->fill_preserve();
				cr->set_source_rgb(0.4, 0.4, 0.4);
				cr->stroke();
				cr->arc(x + 14, y, 4, PI, 3 * PI);
			} break;

			case Lgs::TYPE_XOR: {
				cr->move_to(x - 15, y - 10);
				cr->rel_curve_to(5, 5, 5, 15, 0, 20);
				cr->set_source_rgb(0.4, 0.4, 0.4);
				cr->stroke();
				cr->move_to(x - 10, y - 10);
				cr->rel_curve_to(5, 5, 5, 15, 0, 20);
				cr->rel_curve_to(10, 0, 20, -5, 20, -10);
				cr->rel_curve_to(0, -5, -10, -10, -20, -10);
				cr->close_path();
			} break;

			case Lgs::TYPE_XNOR: {
				cr->move_to(x - 15, y - 10);
				cr->rel_curve_to(5, 5, 5, 15, 0, 20);
				cr->set_source_rgb(0.4, 0.4, 0.4);
				cr->stroke();
				cr->move_to(x - 10, y - 10);
				cr->rel_curve_to(5, 5, 5, 15, 0, 20);
				cr->rel_curve_to(10, 0, 20, -5, 20, -10);
				cr->rel_curve_to(0, -5, -10, -10, -20, -10);
				cr->close_path();
				if (active) {
					cr->set_source_rgb(0.6, 0.7, 1.0);
				}
				else {
					cr->set_source_rgb(0.7, 0.7, 0.7);
				}
				cr->fill_preserve();
				cr->set_source_rgb(0.4, 0.4, 0.4);
				cr->stroke();
				cr->arc(x + 14, y, 4, PI, 3 * PI);
			} break;

			case Lgs::TYPE_NOT: {
				cr->move_to(x + 10, y);
				cr->rel_line_to(-20, -10);
				cr->rel_line_to(0, 20);
				cr->rel_line_to(20, -10);
				cr->arc(x + 14, y, 4, PI, 3 * PI);
			} break;
		}
		if (active) {
			cr->set_source_rgb(0.6, 0.7, 1.0);
		}
		else {
			cr->set_source_rgb(0.7, 0.7, 0.7);
		}
		cr->fill_preserve();
		cr->set_source_rgb(0.4, 0.4, 0.4);
		cr->stroke();
	}

	void draw_cell(
		const Cairo::RefPtr<Cairo::Context>& cr,
		Lgs::Type type,
		int x, int y,
		bool active,
		double size
	) {
		switch (type) {
			case Lgs::TYPE_INPUT: {
				cr->move_to(x - 10 * size, y - 10 * size);
				cr->rel_line_to(0, 20 * size);
				cr->rel_line_to(20 * size, 0);
				cr->rel_line_to(0, -20 * size);
				cr->close_path();
			} break;

			case Lgs::TYPE_OUTPUT: {
				cr->arc(x, y, 10 * size, 0, 2 * PI);
			} break;

			case Lgs::TYPE_AND: {
				cr->move_to(x, y - 10 * size);
				cr->rel_line_to(-10 * size, 0);
				cr->rel_line_to(0, 20 * size);
				cr->arc_negative(x, y, 10 * size, 0.5 * PI, 1.5 * PI);
			} break;

			case Lgs::TYPE_NAND: {
				cr->move_to(x, y - 10 * size);
				cr->rel_line_to(-10 * size, 0);
				cr->rel_line_to(0, 20 * size);
				cr->arc_negative(x, y, 10 * size, 0.5 * PI, 1.5 * PI);
				if (active) {
					cr->set_source_rgb(0.6, 0.7, 1.0);
				}
				else {
					cr->set_source_rgb(0.7, 0.7, 0.7);
				}
				cr->fill_preserve();
				cr->set_source_rgb(0.4, 0.4, 0.4);
				cr->stroke();
				cr->arc(x + 14 * size, y, 4 * size, PI, 3 * PI);
			} break;

			case Lgs::TYPE_OR: {
				cr->move_to(x - 10 * size, y - 10 * size);
				cr->rel_curve_to(5 * size, 5 * size, 5 * size, 15 * size, 0, 20 * size);
				cr->rel_curve_to(10 * size, 0, 20 * size, -5 * size, 20 * size, -10 * size);
				cr->rel_curve_to(0, -5 * size, -10 * size, -10 * size, -20 * size, -10 * size);
				cr->close_path();
			} break;

			case Lgs::TYPE_NOR: {
				cr->move_to(x - 10 * size, y - 10 * size);
				cr->rel_curve_to(5 * size, 5 * size, 5 * size, 15 * size, 0, 20 * size);
				cr->rel_curve_to(10 * size, 0, 20 * size, -5 * size, 20 * size, -10 * size);
				cr->rel_curve_to(0, -5 * size, -10 * size, -10 * size, -20 * size, -10 * size);
				cr->close_path();
				if (active) {
					cr->set_source_rgb(0.6, 0.7, 1.0);
				}
				else {
					cr->set_source_rgb(0.7, 0.7, 0.7);
				}
				cr->fill_preserve();
				cr->set_source_rgb(0.4, 0.4, 0.4);
				cr->stroke();
				cr->arc(x + 14 * size, y, 4 * size, PI, 3 * PI);
			} break;

			case Lgs::TYPE_XOR: {
				cr->move_to(x - 15 * size, y - 10 * size);
				cr->rel_curve_to(5 * size, 5 * size, 5 * size, 15 * size, 0, 20 * size);
				cr->set_source_rgb(0.4, 0.4, 0.4);
				cr->stroke();
				cr->move_to(x - 10 * size, y - 10 * size);
				cr->rel_curve_to(5 * size, 5 * size, 5 * size, 15 * size, 0, 20 * size);
				cr->rel_curve_to(10 * size, 0, 20 * size, -5 * size, 20 * size, -10 * size);
				cr->rel_curve_to(0, -5 * size, -10 * size, -10 * size, -20 * size, -10 * size);
				cr->close_path();
			} break;

			case Lgs::TYPE_XNOR: {
				cr->move_to(x - 15 * size, y - 10 * size);
				cr->rel_curve_to(5 * size, 5 * size, 5 * size, 15 * size, 0, 20 * size);
				cr->set_source_rgb(0.4, 0.4, 0.4);
				cr->stroke();
				cr->move_to(x - 10 * size, y - 10 * size);
				cr->rel_curve_to(5 * size, 5 * size, 5 * size, 15 * size, 0, 20 * size);
				cr->rel_curve_to(10 * size, 0, 20 * size, -5 * size, 20 * size, -10 * size);
				cr->rel_curve_to(0, -5 * size, -10 * size, -10 * size, -20 * size, -10 * size);
				cr->close_path();
				if (active) {
					cr->set_source_rgb(0.6, 0.7, 1.0);
				}
				else {
					cr->set_source_rgb(0.7, 0.7, 0.7);
				}
				cr->fill_preserve();
				cr->set_source_rgb(0.4, 0.4, 0.4);
				cr->stroke();
				cr->arc(x + 14 * size, y, 4 * size, PI, 3 * PI);
			} break;

			case Lgs::TYPE_NOT: {
				cr->move_to(x + 10 * size, y);
				cr->rel_line_to(-20 * size, -10 * size);
				cr->rel_line_to(0, 20 * size);
				cr->rel_line_to(20 * size, -10 * size);
				cr->arc(x + 14 * size, y, 4 * size, PI, 3 * PI);
			} break;
		}
		if (active) {
			cr->set_source_rgb(0.6, 0.7, 1.0);
		}
		else {
			cr->set_source_rgb(0.7, 0.7, 0.7);
		}
		cr->fill_preserve();
		cr->set_source_rgb(0.4, 0.4, 0.4);
		cr->stroke();
	}

	bool draw_cell_input (const Cairo::RefPtr<Cairo::Context>& cr) {
		draw_cell(cr, Lgs::TYPE_INPUT, 20, 20, false, 0.75);
		return true;
	}

	bool draw_cell_output(const Cairo::RefPtr<Cairo::Context>& cr) {
		draw_cell(cr, Lgs::TYPE_OUTPUT, 20, 20, false, 0.75);
		return true;
	}

	bool draw_cell_and   (const Cairo::RefPtr<Cairo::Context>& cr) {
		draw_cell(cr, Lgs::TYPE_AND, 20, 20, false, 0.75);
		return true;
	}

	bool draw_cell_nand  (const Cairo::RefPtr<Cairo::Context>& cr) {
		draw_cell(cr, Lgs::TYPE_NAND, 20, 20, false, 0.75);
		return true;
	}

	bool draw_cell_or    (const Cairo::RefPtr<Cairo::Context>& cr) {
		draw_cell(cr, Lgs::TYPE_OR, 20, 20, false, 0.75);
		return true;
	}

	bool draw_cell_nor   (const Cairo::RefPtr<Cairo::Context>& cr) {
		draw_cell(cr, Lgs::TYPE_NOR, 20, 20, false, 0.75);
		return true;
	}

	bool draw_cell_xor   (const Cairo::RefPtr<Cairo::Context>& cr) {
		draw_cell(cr, Lgs::TYPE_XOR, 20, 20, false, 0.75);
		return true;
	}

	bool draw_cell_xnor  (const Cairo::RefPtr<Cairo::Context>& cr) {
		draw_cell(cr, Lgs::TYPE_XNOR, 20, 20, false, 0.75);
		return true;
	}

	bool draw_cell_not   (const Cairo::RefPtr<Cairo::Context>& cr) {
		draw_cell(cr, Lgs::TYPE_NOT, 20, 20, false, 0.75);
		return true;
	}
}
