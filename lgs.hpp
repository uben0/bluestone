#pragma once
#include <gtkmm.h>
#include <vector>
#include <set>
#include "json.hpp"


namespace Lgs /* Logic Gate Simulator */ {
	typedef unsigned int Id;

	enum Type {
		TYPE_INPUT,
		TYPE_OUTPUT,
		TYPE_AND,
		TYPE_NAND,
		TYPE_OR,
		TYPE_NOR,
		TYPE_XOR,
		TYPE_XNOR,
		TYPE_NOT
	};

	constexpr Id NONE = (Id)(-1);
	Id id_from_json(const Json& json);
	Json id_to_json(Id id);
	const std::string& type_to_string(Type type);

	class Network;

	struct Cell {
		Network* network;
		Type type;
		int x, y;
		bool state[2];
		struct {
			Id id;
			Cell* ptr;
		} inputA, inputB;

		Cell();
		Cell(Network* network, const Cell& src);
		Cell(
			Network* network,
			Type type,
			int x, int y,
			bool state,
			Id inputA,
			Id inputB
		);
		bool get_state() const;
		void set_state(bool state);
		bool is_on(int x, int y) const;
		bool is_in(int x1, int y1, int x2, int y2) const;
		void link_cell_with_ptr();
		bool update();
		Cell& operator=(const Cell& src);
	};

	class bad_id_generation: std::exception {};

	class Network: public std::map<Id, Cell> {
	public:
		unsigned int tray;

		Network();
		Id add(
			Type type,
			int x, int y,
			bool state = false,
			Id inputA = NONE,
			Id inputB = NONE
		);
		void remove(Id id);
		void remove(std::set<Id>& selection);
		void copy(const std::set<Id>& selection, Network& dst);
		void paste(
			const Network& src, int x, int y, std::set<Id>* added = nullptr
		);

		void connect(Id idSrc, Id idDst, Id inputIndex = 0);
		void disconnect_src(Id id);
		void disconnect_dst(Id id, unsigned int inputIndex = 0);

		Id get_new_id() const;
		void link_cells_with_ptr();
		const Cell* get_ptr(Id id) const;
		Cell* get_ptr(Id id);
		unsigned int perform_tick();

		Id find_at_pos(int x, int y) const;
		void get_average_position(int& x, int& y) const;
		void get_average_position(
			const std::set<Id>& selection, int& x, int& y
		) const;
		void set_average_position(int x, int y);
		void set_average_position(const std::set<Id>& selection, int x, int y);
		void shift_position(int x, int y);
		void shift_position(const std::set<Id>& selection, int x, int y);

		bool load_from_json(const Json& json);
		bool save_to_json(Json& json);

	};
}
