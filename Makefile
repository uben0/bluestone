COMPILER = g++
BINARY = bluestone

SOURCES = $(wildcard *.cpp)
HEADERS = $(wildcard *.hpp)
OBJECTS = $(patsubst %.cpp, %.o, $(SOURCES))

FLAGS = -std=c++11 -Wall -Wextra -Werror -fmax-errors=1 `pkg-config --cflags gtkmm-3.0`
LIBS = `pkg-config --libs gtkmm-3.0`

release: $(BINARY)
	./$(BINARY)

debug: FLAGS += -g
debug: $(BINARY)
	gdb ./$(BINARY)

clean:
	rm -f $(OBJECTS) $(BINARY)

$(BINARY): $(OBJECTS)
	$(COMPILER) -o $@ $^ $(LIBS)

%.o: %.cpp $(HEADERS) Makefile
	$(COMPILER) $(FLAGS) -c -o $@ $<
