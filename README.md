# BLUESTONE

A logic gate simulator.
![](https://carlo.uben.ovh/attali/media/bluestone.mp4)

## Features
- Select
- Move
- Copy/Paste
- Save/Load
- Zoom in/Zoom out
