#pragma once
#include <gtkmm.h>
#include "root.hpp"
#include "lgs.hpp"


class Instance: public Gtk::HBox {
public:

	class Data {
	public:
		enum Target {
			TARGET_NONE      = 0,
			TARGET_NETWORK   = 1<< 0,
			TARGET_SELECTION = 1<< 1,
			TARGET_VIEW      = 1<< 2,
			TARGET_GRAB      = 1<< 3
		};

		Root& root;
		sigc::signal<void, Target> signalChanged;
		Lgs::Network                     network;
		std::set<Lgs::Id>              selection;
		std::string                 saveFilePath;
		double                              zoom;
		bool stickToGrid;
		bool curvedConnections;
		bool pointerOnGrab;
		struct {
			int x, y;
		} pointerGrabPos, pointerGrabOrigin, view, grabSelectionVector;
		bool autoTickActive;
		unsigned int autoTickDelay;
		bool isTickStable;
		sigc::connection autoTickConnection;

		Data(Root& root);
		Data(Root& root, const std::string& filePath);
		~Data();
		void perform_tick();
		void set_auto_tick(bool active);
		void toggle_selected_cell_state();
		void save();
		void clear();

		int translate_pos_x_in(int x);
		int translate_pos_y_in(int y);
		int translate_pos_x_out(int x);
		int translate_pos_y_out(int y);

		bool auto_tick_routine();
		void update(Target target);
	} data;


	class Toolbar: public Gtk::VBox {
	public:
		Data& data;

		Gtk::Button cellInput;
		Gtk::Button cellOutput;
		Gtk::Button cellAnd;
		Gtk::Button cellNand;
		Gtk::Button cellOr;
		Gtk::Button cellNor;
		Gtk::Button cellXor;
		Gtk::Button cellXnor;
		Gtk::Button cellNot;

		static void handler_for_cell_input(
			const Glib::RefPtr<Gdk::DragContext>&,
			Gtk::SelectionData& data,
			guint, guint
		) {
			static constexpr Lgs::Type type = Lgs::TYPE_INPUT;
			data.set("Lgs::Type", 8 * sizeof type, (guint8*) &type, sizeof type);
		}
		static void handler_for_cell_output(
			const Glib::RefPtr<Gdk::DragContext>&,
			Gtk::SelectionData& data,
			guint, guint
		) {
			static constexpr Lgs::Type type = Lgs::TYPE_OUTPUT;
			data.set("Lgs::Type", 8 * sizeof type, (guint8*) &type, sizeof type);
		}
		static void handler_for_cell_and(
			const Glib::RefPtr<Gdk::DragContext>&,
			Gtk::SelectionData& data,
			guint, guint
		) {
			static constexpr Lgs::Type type = Lgs::TYPE_AND;
			data.set("Lgs::Type", 8 * sizeof type, (guint8*) &type, sizeof type);
		}
		static void handler_for_cell_nand(
			const Glib::RefPtr<Gdk::DragContext>&,
			Gtk::SelectionData& data,
			guint, guint
		) {
			static constexpr Lgs::Type type = Lgs::TYPE_NAND;
			data.set("Lgs::Type", 8 * sizeof type, (guint8*) &type, sizeof type);
		}
		static void handler_for_cell_or(
			const Glib::RefPtr<Gdk::DragContext>&,
			Gtk::SelectionData& data,
			guint, guint
		) {
			static constexpr Lgs::Type type = Lgs::TYPE_OR;
			data.set("Lgs::Type", 8 * sizeof type, (guint8*) &type, sizeof type);
		}
		static void handler_for_cell_nor(
			const Glib::RefPtr<Gdk::DragContext>&,
			Gtk::SelectionData& data,
			guint, guint
		) {
			static constexpr Lgs::Type type = Lgs::TYPE_NOR;
			data.set("Lgs::Type", 8 * sizeof type, (guint8*) &type, sizeof type);
		}
		static void handler_for_cell_xor(
			const Glib::RefPtr<Gdk::DragContext>&,
			Gtk::SelectionData& data,
			guint, guint
		) {
			static constexpr Lgs::Type type = Lgs::TYPE_XOR;
			data.set("Lgs::Type", 8 * sizeof type, (guint8*) &type, sizeof type);
		}
		static void handler_for_cell_xnor(
			const Glib::RefPtr<Gdk::DragContext>&,
			Gtk::SelectionData& data,
			guint, guint
		) {
			static constexpr Lgs::Type type = Lgs::TYPE_XNOR;
			data.set("Lgs::Type", 8 * sizeof type, (guint8*) &type, sizeof type);
		}
		static void handler_for_cell_not(
			const Glib::RefPtr<Gdk::DragContext>&,
			Gtk::SelectionData& data,
			guint, guint
		) {
			static constexpr Lgs::Type type = Lgs::TYPE_NOT;
			data.set("Lgs::Type", 8 * sizeof type, (guint8*) &type, sizeof type);
		}

		Toolbar(Data& data);
	} toolbar;

	class PlacingArea: public Gtk::DrawingArea {
	public:
		Data& data;
		PlacingArea(Data& data);
		void gates_drag_drop(
			const Glib::RefPtr<Gdk::DragContext>& context,
			int x, int y,
			const Gtk::SelectionData& received,
			guint, guint time
		);
		void update(Data::Target target);

	protected:
		bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);
		bool on_button_press_event  (GdkEventButton* event);
		bool on_button_release_event(GdkEventButton* event);
		bool on_motion_notify_event (GdkEventMotion* event);
		bool on_key_press_event     (GdkEventKey*    event);
		bool on_scroll_event        (GdkEventScroll* event);
	} placingArea;
	Gtk::Frame frame;

	class PropertiesPanel: public Gtk::VBox {
	public:
		Gtk::Label          label;
		Gtk::SpinButton      posX;
		Gtk::SpinButton      posY;
		Gtk::Switch stateSwitcher;
		Gtk::Entry         inputA;
		Gtk::Entry         inputB;
		Gtk::Button        remove;
		Gtk::Separator separator1;
		Gtk::Separator separator2;
		Gtk::HBox         tickBox;
		Gtk::Button   tickPerform;
		Gtk::Button      tickPlay;
		Gtk::SpinButton tickEntry;
		Gtk::CheckButton     grid;
		Gtk::CheckButton    curve;

		void change_pos_x();
		void change_pos_y();
		bool change_state(const GdkEventButton*);
		void change_input_a();
		void change_input_b();
		void change_remove();

		void change_tick_toggle();
		void change_tick_delay();

		void change_stick_grid();
		void change_curve();

		Data& data;

		PropertiesPanel(Data& data);
		void update(Data::Target target);
	} propertiesPanel;

	void action_close();
	void change_file_path(const std::string& filePath);
	sigc::signal<void, Instance*> signalClose;
	Gtk::HBox tabLabel;
	Gtk::Label tabLabelTitle;
	Gtk::Button tabLabelClose;

	Instance(Root& root);
	Instance(Root& root, const std::string& filePath);
};

Instance::Data::Target operator|(
	Instance::Data::Target, Instance::Data::Target
);

Instance::Data::Target& operator|=(
	Instance::Data::Target&, Instance::Data::Target
);
