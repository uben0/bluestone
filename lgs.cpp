#include <fstream>
#include "lgs.hpp"

namespace Lgs {
	Id id_from_json(const Json& json) {
		if (json.is_null()) {
			return NONE;
		}
		else {
			return json.number();
		}
	}
	Json id_to_json(Id id) {
		if (id == NONE) {
			return Json::Null();
		}
		else {
			return Json::Number(id);
		}
	}
	const std::string& type_to_string(Type type) {
		static const std::string typeInput("input");
		static const std::string typeOutput("output");
		static const std::string typeAnd("and");
		static const std::string typeNand("nand");
		static const std::string typeOr("or");
		static const std::string typeNor("nor");
		static const std::string typeXor("xor");
		static const std::string typeXnor("xnor");
		static const std::string typeNot("not");

		switch (type) {
			case TYPE_INPUT:
			return typeInput;

			case TYPE_OUTPUT:
			return typeOutput;

			case TYPE_AND:
			return typeAnd;

			case TYPE_NAND:
			return typeNand;

			case TYPE_OR:
			return typeOr;

			case TYPE_NOR:
			return typeNor;

			case TYPE_XOR:
			return typeXor;

			case TYPE_XNOR:
			return typeXnor;

			case TYPE_NOT:
			return typeNot;
		}

		throw std::exception();
	}

	Cell::Cell():
		network(nullptr)
	{}

	Cell::Cell(Network* network, const Cell& src):
		network(network),
		type(src.type),
		x(src.x),
		y(src.y),
		state{src.state[0], src.state[1]},
		inputA{src.inputA.id, nullptr},
		inputB{src.inputB.id, nullptr}
	{}

	Cell::Cell(
		Network* network,
		Type type,
		int x, int y,
		bool state,
		Id inputA,
		Id inputB
	):
		network(network),
		type(type),
		x(x),
		y(y),
		state{state, state},
		inputA{inputA, nullptr},
		inputB{inputB, nullptr}
	{}

	bool Cell::is_on(int x, int y) const {
		return (
			x > this->x - 12 &&
			x < this->x + 12 &&
			y > this->y - 12 &&
			y < this->y + 12
		);
	}

	bool Cell::is_in(int x1, int y1, int x2, int y2) const {
		if (x1 > x2) std::swap(x1, x2);
		if (y1 > y2) std::swap(y1, y2);
		return x2 > x - 12 && x1 < x + 12 && y2 > y - 12 && y1 < y + 12;
	}

	bool Cell::get_state() const {
		return state[network->tray];
	}

	void Cell::set_state(bool state) {
		this->state[0] = state;
		this->state[1] = state;
	}

	void Cell::link_cell_with_ptr() {
		inputA.ptr = network->get_ptr(inputA.id);
		if (inputA.ptr == nullptr) {
			inputA.id = NONE;
		}
		inputB.ptr = network->get_ptr(inputB.id);
		if (inputB.ptr == nullptr) {
			inputB.id = NONE;
		}
	}

	bool Cell::update() {
		switch (type) {

			case TYPE_INPUT: {
			} break;

			case TYPE_OUTPUT: {
				state[network->tray] = (
					inputA.ptr != nullptr and
					inputA.ptr->state[1 - network->tray]
				);
			} break;

			case TYPE_AND: {
				state[network->tray] = (
					(
						inputA.ptr != nullptr and
						inputA.ptr->state[1 - network->tray]
					) and (
						inputB.ptr != nullptr and
						inputB.ptr->state[1 - network->tray]
					)
				);
			} break;

			case TYPE_NAND: {
				state[network->tray] = not (
					(
						inputA.ptr != nullptr and
						inputA.ptr->state[1 - network->tray]
					) and (
						inputB.ptr != nullptr and
						inputB.ptr->state[1 - network->tray]
					)
				);
			} break;

			case TYPE_OR: {
				state[network->tray] = (
					(
						inputA.ptr != nullptr and
						inputA.ptr->state[1 - network->tray]
					) or (
						inputB.ptr != nullptr and
						inputB.ptr->state[1 - network->tray]
					)
				);
			} break;

			case TYPE_NOR: {
				state[network->tray] = not (
					(
						inputA.ptr != nullptr and
						inputA.ptr->state[1 - network->tray]
					) or (
						inputB.ptr != nullptr and
						inputB.ptr->state[1 - network->tray]
					)
				);
			} break;

			case TYPE_XOR: {
				state[network->tray] = (
					(
						inputA.ptr != nullptr and
						inputA.ptr->state[1 - network->tray]
					) != (
						inputB.ptr != nullptr and
						inputB.ptr->state[1 - network->tray]
					)
				);
			} break;

			case TYPE_XNOR: {
				state[network->tray] = (
					(
						inputA.ptr != nullptr and
						inputA.ptr->state[1 - network->tray]
					) == (
						inputB.ptr != nullptr and
						inputB.ptr->state[1 - network->tray]
					)
				);
			} break;

			case TYPE_NOT: {
				state[network->tray] = not (
					inputA.ptr != nullptr and
					inputA.ptr->state[1 - network->tray]
				);
			} break;
		}
		return state[0] != state[1];
	}

	Cell& Cell::operator=(const Cell& src) {
		network    = src.network;
		type       = src.type;
		x          = src.x;
		y          = src.y;
		state[0]   = src.state[0];
		state[1]   = src.state[1];
		inputA.id  = src.inputA.id;
		inputA.ptr = src.inputA.ptr;
		inputB.id  = src.inputB.id;
		inputB.ptr = src.inputB.ptr;

		return *this;
	}

	Network::Network() {
		std::srand(time(nullptr));
		tray = 0;
	}

	const Cell* Network::get_ptr(Id id) const {
		if (id == NONE) return nullptr;

		auto it = find(id);
		if (it == end()) {
			return nullptr;
		}
		else {
			return &it->second;
		}
	}

	Cell* Network::get_ptr(Id id) {
		if (id == NONE) return nullptr;

		auto it = find(id);
		if (it == end()) {
			return nullptr;
		}
		else {
			return &it->second;
		}
	}

	Id Network::get_new_id() const {
		Id id = NONE;
		unsigned int mask = size();
		for (
			unsigned int shift = 1;
			shift < sizeof(unsigned int) * 8;
			shift = shift << 1
		) mask |= mask >> shift;
		unsigned int count = 0;
		while (id == NONE or find(id) != end()) {
			id = rand() & mask;
			if (count % 8) mask = (mask << 1) + 1;
			if (count >= 64) throw bad_id_generation();
			count++;
		}
		return id;
	}

	Id Network::add(
		Type type,
		int x, int y,
		bool state,
		Id inputA,
		Id inputB
	) {
		Id id = get_new_id();
		operator[](id) = Cell(
			this, type, x, y, state,
			inputA, inputB
		);
		return id;
	}

	void Network::remove(Id id) {
		if (find(id) != end()) {
			disconnect_src(id);
			erase(id);
		}
	}

	void Network::remove(std::set<Id>& selection) {
		for (Id id : selection) {
			remove(id);
		}
		selection.clear();
	}

	void Network::copy(const std::set<Id>& selection, Network& dst) {
		dst.clear();
		for (Id id : selection) {
			if (find(id) != end()) {
				dst[id] = Lgs::Cell(&dst, at(id));
			}
		}
		dst.link_cells_with_ptr();
		int x, y;
		dst.get_average_position(x, y);
		dst.shift_position(-x, -y);
	}

	void Network::paste(const Network& src, int x, int y, std::set<Id>* added) {
		std::map<Id, Id> translate;
		for (auto& it : src) {
			translate[it.first] = add(
				it.second.type,
				it.second.x + x,
				it.second.y + y,
				it.second.get_state(),
				it.second.inputA.id,
				it.second.inputB.id
			);
		}
		for (auto& it : src) {
			Cell& cell = at(translate[it.first]);
			if (cell.inputA.id != NONE) {
				cell.inputA.id = translate[cell.inputA.id];
			}
			if (cell.inputB.id != NONE) {
				cell.inputB.id = translate[cell.inputB.id];
			}
		}
		link_cells_with_ptr();

		if (added != nullptr) {
			added->clear();
			for (auto it : translate) {
				added->insert(it.second);
			}
		}
	}


	void Network::connect(
		Id srcId, Id dstId, unsigned int inputIndex
	) {
		Cell* srcPtr = get_ptr(srcId);
		Cell* dstPtr = get_ptr(dstId);
		if (srcPtr == nullptr or dstPtr == nullptr) return;

		switch (dstPtr->type) {
			case TYPE_INPUT: {
			} break;

			case TYPE_OUTPUT:
			case TYPE_NOT: {
				if (inputIndex == 0) {
					dstPtr->inputA.id  = srcId;
					dstPtr->inputA.ptr = srcPtr;
				}
			} break;

			case TYPE_AND:
			case TYPE_NAND:
			case TYPE_OR:
			case TYPE_NOR:
			case TYPE_XOR:
			case TYPE_XNOR: {
				if (inputIndex == 0) {
					dstPtr->inputA.id  = srcId;
					dstPtr->inputA.ptr = srcPtr;
				}
				else if (inputIndex == 1) {
					dstPtr->inputB.id  = srcId;
					dstPtr->inputB.ptr = srcPtr;
				}
			} break;
		}
	}

	void Network::disconnect_src(Id id) {
		if (id != NONE) {
			for (iterator it = begin(); it != end(); it++) {
				if (it->second.inputA.id == id) {
					it->second.inputA.id  = NONE;
					it->second.inputA.ptr = nullptr;
				}
				if (it->second.inputB.id == id) {
					it->second.inputB.id  = NONE;
					it->second.inputB.ptr = nullptr;
				}
			}
		}
	}

	void Network::disconnect_dst(Id id, unsigned inputIndex) {
		Cell* cell = get_ptr(id);
		if (cell != nullptr) {
			if (inputIndex == 0) {
				cell->inputA.id  = NONE;
				cell->inputA.ptr = nullptr;
			}
			else if (inputIndex == 1) {
				cell->inputB.id  = NONE;
				cell->inputB.ptr = nullptr;
			}
		}
	}

	Id Network::find_at_pos(int x, int y) const {
		for (auto it = begin(); it != end(); it++) {
			if (it->second.is_on(x, y)) return it->first;
		}
		return NONE;
	}

	void Network::get_average_position(int& x, int& y) const {
		if (size() == 0) {
			x = 0;
			y = 0;
			return;
		}
		int xSum = 0, ySum = 0;
		for (auto& it : *this) {
			xSum += it.second.x;
			ySum += it.second.y;
		}
		x = xSum / size();
		y = ySum / size();
	}

	void Network::set_average_position(int x, int y) {
		int currentX, currentY;
		get_average_position(currentX, currentY);
		shift_position(x - currentX, y - currentY);
	}

	void Network::get_average_position(
		const std::set<Id>& selection, int& x, int& y
	) const {
		int count = 0;
		int xSum = 0;
		int ySum = 0;
		for (Id id : selection) {
			const Cell* cell = get_ptr(id);
			if (cell != nullptr) {
				xSum += cell->x;
				ySum += cell->y;
				count++;
			}
		}
		if (count == 0) {
			x = 0;
			y = 0;
			return;
		}
		x = xSum / count;
		y = ySum / count;
	}

	void Network::set_average_position(
		const std::set<Id>& selection, int x, int y
	) {
		int currentX, currentY;
		get_average_position(selection, currentX, currentY);
		shift_position(selection, x - currentX, y - currentY);
	}

	void Network::shift_position(int x, int y) {
		for (auto& it : *this) {
			it.second.x += x;
			it.second.y += y;
		}
	}

	void Network::shift_position(const std::set<Id>& selection, int x, int y) {
		for (Id id : selection) {
			Cell* cell = get_ptr(id);
			if (cell != nullptr) {
				cell->x += x;
				cell->y += y;
			}
		}
	}

	unsigned int Network::perform_tick() {
		tray = 1 - tray;
		unsigned int sum = 0;
		for (iterator it = begin(); it != end(); it++) {
			sum += (unsigned int) it->second.update();
		}
		// std::cout << "tick performed with " << sum;
		// if (sum <= 1) {
		// 	std::cout << " update" << std::endl;
		// }
		// else {
		// 	std::cout << " updates" << std::endl;
		// }
		return sum;
	}

	void Network::link_cells_with_ptr() {
		for (auto it = begin(); it != end(); it++) {
			it->second.link_cell_with_ptr();
		}
	}

	bool Network::load_from_json(const Json& json) {
		clear();
		try {
			const Json::Array& array = json.array();

			for (auto it = array.begin(); it != array.end(); it++) {
				const Json::String& type = (*it)["type"].string();

				if (type == "input") {
					operator[]((*it)["id"].number()) = Cell(
						this,
						TYPE_INPUT,
						(*it)["x"].number(),
						(*it)["y"].number(),
						(*it)["state"].boolean(),
						NONE,
						NONE
					);
				}
				else if (type == "output") {
					operator[]((*it)["id"].number()) = Cell(
						this,
						TYPE_OUTPUT,
						(*it)["x"].number(),
						(*it)["y"].number(),
						(*it)["state"].boolean(),
						id_from_json((*it)["input"]),
						NONE
					);
				}
				else if (type == "and") {
					operator[]((*it)["id"].number()) = Cell(
						this,
						TYPE_AND,
						(*it)["x"].number(),
						(*it)["y"].number(),
						(*it)["state"].boolean(),
						id_from_json((*it)["input A"]),
						id_from_json((*it)["input B"])
					);
				}
				else if (type == "nand") {
					operator[]((*it)["id"].number()) = Cell(
						this,
						TYPE_NAND,
						(*it)["x"].number(),
						(*it)["y"].number(),
						(*it)["state"].boolean(),
						id_from_json((*it)["input A"]),
						id_from_json((*it)["input B"])
					);
				}
				else if (type == "or") {
					operator[]((*it)["id"].number()) = Cell(
						this,
						TYPE_OR,
						(*it)["x"].number(),
						(*it)["y"].number(),
						(*it)["state"].boolean(),
						id_from_json((*it)["input A"]),
						id_from_json((*it)["input B"])
					);
				}
				else if (type == "nor") {
					operator[]((*it)["id"].number()) = Cell(
						this,
						TYPE_NOR,
						(*it)["x"].number(),
						(*it)["y"].number(),
						(*it)["state"].boolean(),
						id_from_json((*it)["input A"]),
						id_from_json((*it)["input B"])
					);
				}
				else if (type == "xor") {
					operator[]((*it)["id"].number()) = Cell(
						this,
						TYPE_XOR,
						(*it)["x"].number(),
						(*it)["y"].number(),
						(*it)["state"].boolean(),
						id_from_json((*it)["input A"]),
						id_from_json((*it)["input B"])
					);
				}
				else if (type == "xnor") {
					operator[]((*it)["id"].number()) = Cell(
						this,
						TYPE_XNOR,
						(*it)["x"].number(),
						(*it)["y"].number(),
						(*it)["state"].boolean(),
						id_from_json((*it)["input A"]),
						id_from_json((*it)["input B"])
					);
				}
				else if (type == "not") {
					operator[]((*it)["id"].number()) = Cell(
						this,
						TYPE_NOT,
						(*it)["x"].number(),
						(*it)["y"].number(),
						(*it)["state"].boolean(),
						id_from_json((*it)["input"]),
						NONE
					);
				}
			}
		}
		catch (Json::bad_syntax&) {
			return false;
		}
		catch (Json::bad_type&) {
			return false;
		}
		link_cells_with_ptr();
		return true;
	}

	bool Network::save_to_json(Json& json) {
		json = Json::Array();
		Json::Array& array = json.array();

		for (iterator it = begin(); it != end(); it++) {
			switch (it->second.type) {
				case TYPE_INPUT: {
					array.push_back(Json::Object({
						{"id", Json::Number(it->first)},
						{"type", type_to_string(it->second.type)},
						{"x", Json::Number(it->second.x)},
						{"y", Json::Number(it->second.y)},
						{"state", Json::Boolean(it->second.get_state())}
					}));
				} break;
				case TYPE_OUTPUT:
				case TYPE_NOT: {
					array.push_back(Json::Object({
						{"id", Json::Number(it->first)},
						{"type", type_to_string(it->second.type)},
						{"x", Json::Number(it->second.x)},
						{"y", Json::Number(it->second.y)},
						{"state", Json::Boolean(it->second.get_state())},
						{"input", id_to_json(it->second.inputA.id)}
					}));
				} break;
				case TYPE_AND:
				case TYPE_NAND:
				case TYPE_OR:
				case TYPE_NOR:
				case TYPE_XOR:
				case TYPE_XNOR: {
					array.push_back(Json::Object({
						{"id", Json::Number(it->first)},
						{"type", type_to_string(it->second.type)},
						{"x", Json::Number(it->second.x)},
						{"y", Json::Number(it->second.y)},
						{"state", Json::Boolean(it->second.get_state())},
						{"input A", id_to_json(it->second.inputA.id)},
						{"input B", id_to_json(it->second.inputB.id)}
					}));
				} break;
			}
		}
		return true;
	}
}
